variable "url_map" {
  type        = string
  description = "A reference to the UrlMap resource that defines the mapping from URL to the BackendService"
}

variable "hosts" {
  type        = list(string)
  description = "List of hostnames"
}

variable "service_ports" {
  type        = list(string)
  description = "ports to allow for healthchecks"
}

variable "dns_zone_prefix" {
  type        = string
  description = "To prefix a zone. E.g. set to 'ops.'"
  default     = ""
}

variable "dns_zone_name" {
  type        = string
  description = "Name of the DNS zone (to suffix to the hostnames) (AWS)"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "name" {
  type        = string
  description = "Name of the load balancer; ends up as part of the name of various GCP objects"
}

variable "targets" {
  type        = list(string)
  description = "target tags for the load balancer"
}

variable "ssl_policy" {
  type        = string
  description = "ssl policy to associate with this load balancer"
  default     = ""
}