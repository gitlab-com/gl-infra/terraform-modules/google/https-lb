module "dns_record" {
  source  = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "3.17.0"

  zone = "${var.dns_zone_name}."

  a = { for host in var.hosts :
    "${host}.${var.dns_zone_prefix}${var.dns_zone_name}." => {
      records = [google_compute_global_address.default.address]
      ttl     = 300
    }
  }
}
