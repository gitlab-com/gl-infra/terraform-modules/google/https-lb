resource "google_compute_managed_ssl_certificate" "default" {
  name = format("%v-%v", var.environment, var.name)

  managed {
    domains = [for host in var.hosts :
      "${host}.${var.dns_zone_prefix}${var.dns_zone_name}"
    ]
  }
}
