resource "google_compute_firewall" "default" {
  name = format("%v-%v", var.environment, var.name)

  network     = var.environment
  target_tags = var.targets

  allow {
    protocol = "tcp"
    ports    = var.service_ports
  }

  source_ranges = concat(
    data.google_compute_lb_ip_ranges.ranges.network,
    data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal,
  )
}
