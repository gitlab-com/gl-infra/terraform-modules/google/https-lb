resource "google_compute_global_address" "default" {
  name = format("%v-%v", var.environment, var.name)
}

resource "google_compute_target_https_proxy" "default" {
  name = format("%v-%v", var.environment, var.name)

  ssl_certificates = [google_compute_managed_ssl_certificate.default.id]
  ssl_policy       = var.ssl_policy == "" ? null : var.ssl_policy
  url_map          = var.url_map
}

resource "google_compute_global_forwarding_rule" "default" {
  name = format("%v-%v", var.environment, var.name)

  target     = google_compute_target_https_proxy.default.self_link
  port_range = "443"
  ip_address = google_compute_global_address.default.address
}